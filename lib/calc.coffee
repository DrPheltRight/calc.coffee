Calc =
  sum: (a, b) -> a + b
  diff: (a, b) -> a - b
  product: (a, b) -> a * b
  quotient: (a, b) -> a / b

module.exports = Calc
