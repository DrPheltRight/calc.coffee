Calc = require '../lib/calc'

describe 'Calc', ->
  describe '.sum', ->
    it 'should return 2 given 1 and 1', ->
      expect(Calc.sum(1, 1)).toEqual(2)

  describe '.diff', ->
    it 'should return 3 given 5 and 2', ->
      expect(Calc.diff(5, 2)).toEqual(3)

  describe '.product', ->
    it 'should return 12 given 3 and 4', ->
      expect(Calc.product(3, 4)).toEqual(12)

  describe '.quotient', ->
    it 'should return 4 given 12 and 3', ->
      expect(Calc.quotient(12, 3)).toEqual(4)
